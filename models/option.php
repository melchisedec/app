<?php



require_once( 'Medoo.php' );


class Option extends DbConnect{

    private $table = "options";
    

    public function __construct(){
        parent::__construct();

    }



    //adding a record to option table
    public  function add( $name, $value, $remote_id=null, $autoload=false ){
            $this->db->insert( $this->table, [ 
                                        'name'=> $name,
                                        'value'=> $value,
                                        'remote_id' => $remote_id,
                                        'autoload' => $autoload
                                             ] );

            return $this->db->id() > 0 ? true : false;
    }



    //update options
    public  function update( $identifier, $field, $newvalue ){

             $column  =  ( int ) $identifier ? 'id' : 'name';

             $update =  $this->db->update( $this->table ,[ $field => $newvalue
                                                             ], [
                                                             $column => $identifier
                                                             ] );
                                                 

             return $update > 0  ? true : false;
    }

    

    //obtain records
    public function get( $identifier=null ){

        if( $identifier!=null ):
            $column  =  ( int ) $identifier  ? 'id' : 'name';


             $records = $this->db->select( $this->table , [
                                            'id','name','value'
                                            ], [
                                            $column => $identifier, 
                                             'LIMIT'=>1 
                                                 ] );
            return !empty( $records) ?  $records[0] : [];
            
            else:
            
              $records = $this->db->select( $this->table , '*' );
              return !empty( $records) ?  json_encode( $records ) : [];

            endif;

                
                
    }

    //delete a record from a table
    public function delete( $identifier ){
             $column  =  is_numeric( $identifier ) ? 'id' : 'name';

             $delete =  $this->db->delete( $this->table, [ 
                                                        $column=> $identifier
                                                             ] );
                                                 

             return ( $delete > 0 ) ? true : false;
    }




     //pagination
    public function get_records( $page,$per_page ){

        //pagination object
        $pagination = ( object ) [
                'per_page'=> isset( $per_page ) ? ( int ) $per_page : 4,
                'page' => isset( $page ) && ($page > 0 ) ? ( int ) $page : 1
           ];

        //limit
         $limit = ( object )[ 
                        'limit_1' => ( $pagination->page - 1 ) * $pagination->per_page, 
                        'limit_2' => $pagination->per_page
                 ];

        
         $results =  $this->db->select( $this->table, 
                                ['id','name','value'],
                                ['LIMIT'=> [$limit->limit_1, $limit->limit_2 ] ] ); 

                return isset( $results ) ? json_encode( $results ): [];  
    
        
    }


    



}

?>