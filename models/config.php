<?php


require_once( 'Medoo.php' );

class DbConnect{

    private $dbconfig =   [ 'database_type' => 'mysql',
                'database_name' => 'mydb',
                'server' => 'localhost',
                'username' => 'root',
                'password' => '',
                'charset' => 'utf8' ] ;
    public $db;

    public function __construct(){
        $this->db = new Medoo( $this->dbconfig );
        
    }

    
 }